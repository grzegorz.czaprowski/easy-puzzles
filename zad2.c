#include <stdio.h>

//Zadanie 2. Wczytać od użytkownika 3 liczby całkowite i wypisać na
//        ekran największą oraz najmniejszą z nich.

int main() {
    printf("enter 3 integer numbers:\n");
    int a, b, c;
    scanf("%d %d %d", &a, &b, &c);

    int min;
    int max;
/*
    if (a >= b) {
        max = a;
        min = b;
    } else {
        max = b;
        min = a;
    }

    if (c > max) {
        max = c;
    } else if (c < min) {
        min = c;
    }*/

    max = (a >= b ? a : b);
    min = (a >= b ? b : a);
    max = (c >= max ? c : max);
    min = (c >= min ? min : c);

    printf("highest number is %d and lowest number is %d", max, min);
    return 0;
}