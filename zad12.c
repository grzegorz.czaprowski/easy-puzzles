/*
Zadanie 12. Gra w ”Za dużo, za mało”. Komputer losuje liczbę z
zakresu 1...100, a gracz (użytkownik) ma za zadanie odgadnąć, co to
        za liczba poprzez podawanie kolej- nych wartości. Jeżeli podana
wartość jest:
• większa – wyświetlany jest komunikat „Podałeś za dużą wartość”,
• mniejsza – wyświetlany jest komunikat „Podałeś za małą wartość”,
• identyczna z wylosowaną – wyświetlany jest komunikat
„Gratulacje” i gra się kończy. */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    printf("try to guess which number from 1 to 100 I drawed:\n");
    time_t tt;
    srand(time(&tt));
    int num = (rand() % 100 + 1);
    int guess;
    printf("%d", num);

    while (1) {
        scanf("%d", &guess);
        if (guess > num) {
            printf("Your guess is too big, try again\n");
        } else if (guess < num) {
            printf("Your guess is too small, try again\n");
        } else {
            printf("Congratulations, you guessed right!\n");
            break;
        }
    }

    return 0;
}
