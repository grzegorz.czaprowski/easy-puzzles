//Zadanie 6. Napisać program realizujący funkcje prostego kalkulatora,
//        pozwalającego na wy- konywanie operacji dodawania, odejmowania,
//mnożenia i dzielenia na dwóch licz- bach rzeczywistych. Program ma
//identyfikować sytuację wprowadzenia błędnego symbolu działania
//oraz próbę dzielenia przez zero. Zastosować instrukcję switch do
//wykonania odpowiedniego działania w zależności od
//wprowadzonego symbolu operacji. Scenariusz działania programu:
//a) Program wyświetla informację o swoim przeznaczeniu.
//b) Wczytuje pierwszą liczbę.
//c) Wczytuje symbol operacji arytmetycznej: +, -, *, /.
//d) Wczytuje drugą liczbę.
//e) Wyświetla wynik lub - w razie konieczności - informację o niemożności
//        wy- konania działania.
//f) Program kończy swoje działanie po naciśnięciu przez użytkownika
//klawisza Enter.

#include <stdio.h>

//czysci bufor scanf
void clear(void) {
    while (getchar() != '\n');
}


int main() {
    double numA;
    double numB;
    char operator;

    printf("enter your operation (ex. 1+1):\n");
    scanf("%lf %c %lf", &numA, &operator, &numB);

    switch (operator) {
        case '+':
            printf("%lf\n", numA + numB);
            break;
        case '-':
            printf("%lf\n", numA - numB);
            break;
        case '*':
            printf("%lf\n", numA * numB);
            break;
        case '/':
            if (numB == 0) {
                printf("You can't divide by 0\n");
                break;
            }
            printf("%lf\n", numA / numB);
            break;
        default:
            printf("bad operator\n");
            break;
    }

    do {
        printf("pres ENTER to end the program\n");
        clear();
        int c = getchar();
        if (c == '\n') {
            return 0;
        }
    } while(1);

}