//Zadanie 3. Napisać program, który oblicza wartość współczynnika
//BMI (ang. body mass index) wg. wzoru: waga/wzrost^2 . Jeżeli wynik
//jest w przedziale (18,5 - 24,9) to wypisuje
//”waga prawidłowa”, jeżeli poniżej to ”niedowaga”, jeżeli powyżej
//”nadwaga”.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

char *whichCategory(double bmi) {
    if (bmi > 24.9) {
        return "overweight\n";
    } else if (bmi < 18.5) {
        return "underweight\n";
    } else {
        return "ok\n";
    }
}

int main() {
    double height, weight;
    printf("Enter your height (in meters) and weight (in kilograms):\n");
    scanf("%lf %lf", &height, &weight);

    double bmi = weight / pow(height, 2);
    printf("Your Body Mass Index equals %lf and you are %s.\n", bmi, whichCategory(bmi));
    return 0;
}