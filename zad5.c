//Zadanie 5. W sklepie ze sprzętem AGD oferowana jest sprzedaż
//ratalna. Napisz program umożliwiający wyliczenie wysokości
//miesięcznej raty za zakupiony sprzęt. Danymi wejściowymi dla
//programu są:
//• cena towaru (od 100 zł do 10 tyś. zł), • liczba rat (od 6 do 48).
//Kredyt jest oprocentowany w zależności od liczby rat:
//• od 6–12 wynosi 2.5% , • od 13–24 wynosi 5%, • od 25–48 wynosi
//10%.
//Obliczona miesięczna rata powinna zawierać również odsetki.
//Program powinien sprawdzać, czy podane dane mieszczą się w
//określonych powyżej zakresach, a w przypadku błędu pytać prosić
//użytkownika ponownie o podanie danych.

#include <stdio.h>

int main() {
    printf("enter item's price(from 100 too 10000) and number of installments (from 6 to 48):\n");
    double price;
    int installments;
    int true;
    do {
        true = 1;
        scanf("%lf %d", &price, &installments);
        if (price > 10000 || price < 100) {
            printf("wrong price, try again\n");
            true = 0;
        } else if (installments < 6 || installments > 48) {
            printf("wrong number of installments, try again\n");
            true = 0;
        }
    } while (true == 0);

    double singleinstallment;

    if (installments < 13) {
        singleinstallment = price * 1.025 / installments;
    } else if (installments < 25) {
        singleinstallment = price * 1.05 / installments;
    } else {
        singleinstallment = price * 1.10 / installments;
    }

    printf("Single installment of product equals %.2lf zl", singleinstallment);

    return 0;
}