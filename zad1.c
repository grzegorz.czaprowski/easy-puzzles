#include <stdio.h>

/*
Zadanie 1. Napisać program służący do konwersji wartości
temperatury podanej w stopniach Celsjusza na stopnie w skali
Fahrenheita (stopnie Fahrenheita = 1.8 * stopnie Celsjusza + 32.0)
*/
int main() {
    double c;
    printf("enter temperature in Celsius degrees:\n");
    scanf("%lf", &c);

    c = 1.8 * c + 32;

    printf("In Fahrenheit: %lf", c);
    return 0;
}