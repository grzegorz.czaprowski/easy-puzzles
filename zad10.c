/*
Zadanie 10. Napisać program, który wczytuje liczby podawane przez
użytkownika dotąd, do- póki nie podana zostanie liczba 0. Następnie
        wyświetlić sumę wszystkich poda- nych liczb. */

#include <stdlib.h>
#include <stdio.h>

int main() {
    printf("enter your numbers, if you want to stop enter 0\n");
    double num;
    double sum = 0;

    do {
        scanf("%lf", &num);

        sum += num;
    } while (num != 0);

    printf("the sum of all numbers equals %.2lf\n", sum);

    return 0;
}