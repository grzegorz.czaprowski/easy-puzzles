//Zadanie 4. Napisać program obliczający należny podatek
//        dochodowy od osób fizycznych. Program ma pobierać od
//użytkownika dochód i po obliczeniu wypisywać na ekranie należny
//        podatek. Podatek obliczany jest wg. następujących reguł:
//• do 85.528 podatek wynosi 18% podstawy minus 556,02 PLN,
//• od 85.528 podatek wynosi 14.839,02 zł + 32% nadwyżki ponad
//85.528,00

#include <stdio.h>

int main() {
    printf("enter your income:\n");
    double income;
    scanf("%lf", &income);
    double tax;

    if (income <= 3089) {
        printf("You don't need to pay taxes!\n");
    } else if (income <= 85528) {
        tax = income * 0.18 - 556.02;
        printf("Tax you need to pay is %.2lf zl\n", tax);
    } else {
        tax = 14839.02 + (income - 85528) * 0.32;
        printf("Tax you need to pay is %.2lf zl\n", tax);
    }

    return 0;
}