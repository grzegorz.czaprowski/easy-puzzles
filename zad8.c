//Zadanie 8. Napisać program pobierający od użytkownika dwie liczby
//        całkowite A oraz B, A < B, a następnie wyznaczający sumę ciągu
//        liczb od A do B, czyli sumę ciągu (A, A + 1, . . . , B). Obliczenia
//        należy wykonać trzykrotnie stosując kolejno pętle: while, do-while,
//for.
//Przykład:
//Dla A = 4 i B = 11 program powinien wyświetlić:
//60 60 60

#include <stdio.h>
#include <stdlib.h>

int main() {
    printf("enter two integral numbers, second need to be bigger than first:\n");
    int numA;
    int numB;
    int sum = 0;
    int true = 0;

    while (true == 0) {
        scanf("%d %d", &numA, &numB);
        if (numA >= numB) {
            printf("second number is not bigger than first, try again\n");
        } else {
            true = 1;
        }
    }

    for (int i = numA; i <= numB; i++) {
        sum += i;
    }
    printf("%d ", sum);

    sum = 0;

    int count = numA;
    while (count <= numB) {
        sum += count;
        count++;
    }
    printf("%d ", sum);

    sum = 0;
    count = numA;
    do {
        sum += count;
        count++;
    } while (count <= numB);
    printf("%d ", sum);

    return 0;
}