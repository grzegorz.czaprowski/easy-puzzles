/*
Zadanie 9. Napisać program, który wczytuje od użytkownika liczbę
całkowitą dodatnią n, a następnie wyświetla na ekranie wszystkie
potęgi liczby 2 nie większe, niż podana liczba. Przykładowo, dla
        liczby 71 program powinien wyświetlić:
1 2 4 8 16 32 64 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
    int num;
    int true = 0;

    printf("enter integral positive number:\n");

    while (true == 0) {
        scanf("%d", &num);
        if (num > 0) {
            true = 1;
        } else {
            printf("incorect number, try again:\n");
        }
    }

    double powersOfTwo = 0;
    int i = 0;
    while (1) {
        powersOfTwo = pow(2, i);
        if (powersOfTwo < num) {
            printf("%.0lf ", powersOfTwo);
            i++;
            continue;
        }
        break;
    }

    return 0;
}