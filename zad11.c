/*
Zadanie 11A. Napisać program, który pobiera od użytkownika ciąg
liczb całkowitych. Pobieranie danych kończone jest podaniem
        wartości 0 (nie wliczana do danych). W następ- nej kolejności
program powinien wyświetlić sumę największej oraz najmniejszej z
podanych liczb oraz ich średnią arytmetyczną.
 Przykład:
Użytkownik podał ciąg: 1, -4, 2, 17, 0. Wynik programu:
 13 // suma min. i maks.
 6.5 // średnia
11.B posortuj ciąg liczb od najwiekszej do najmniejszej
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

void variantA() {
    printf("enter your numbers, if you want to stop enter 0\n");
    int num;
    int min = INT_MAX;
    int max = INT_MIN;

    while (1) {
        scanf("%d", &num);
        if (num == 0) {
            break;
        }
        if (num >= max) {
            max = num;
        }
        if (num <= min) {
            min = num;
        }
    }

    printf("\n%d %.2lf", max + min, (double) (max + min) / 2);
}

void variantB(){
    printf("enter your numbers, if you want to stop enter 0\n");
    int num;
    int array[500];
    int array2[500];
    int i = 0;

    while (1) {
        scanf("%d", &num);
        if (num == 0) {
            break;
        }
        array[i] = num;
        i++;
    }

    for (int k = 0; k < i; k++) {
        int checkedNumber = array[k];
        int count = 0;
        for (int x = 0; x < i; x++) {
            if (checkedNumber < array[x]) {
                count++;
            }
        }
        array2[count] = checkedNumber;
    }

    for (int j = 0; j < i; j++) {
        printf("\n%d", array2[j]);
    }
}

int main() {
    variantA();
    variantB();

    return 0;
}
