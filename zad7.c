//Zadanie 7. Napisać program, który pobiera od użytkownika liczbę
//całkowitą dodatnią, a na- stępnie wyświetla na ekranie kolejno
//        wszystkie liczby niepatrzyste nie większe od podanej liczby.
//Przykład, dla 15 program powinien wyświetlić 1, 3, 5, 7, 9, 11, 13,
//15.

#include <stdlib.h>
#include <stdio.h>

int main() {
    int num;
    int true = 0;

    printf("enter integral positive number:\n");

    while (true == 0) {
        scanf("%d", &num);
        if (num > 0) {
            true = 1;
        } else {
            printf("incorect number, try again:\n");
        }
    }

    for (int i = 0; i <= num; i++) {
        if (i % 2 == 1) {
            printf("%d ", i);
        }
    }

    return 0;
}